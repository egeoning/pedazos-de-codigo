# Pedazos de código

Trozos de código que me han resultado de utilidad en mis proyectos

# Comparar matrices para actualizar, insertar y/o añadir.
En mi último proyecto, en el lado del cliente, se incluíía una pantalla que permite editar, borrar y añadir ciertas entidades.

Como es muy compleja y el nro. de operaciones RUD podríía llegar a ser elevado, opté por no hacer una peticióón REST para cada una. Eso me obligaba a guardar el control de cada uno de los cambios.

Para ello utilizo dos matrices de objetos:
    * datos originales -los cargados al cargar la páágina-
    * datos modificados -los recopilados al pulsar el botón "Guardar cambios"

Nótese que digo "datos recopilados". Eso significa que no utilizo un formulario para los campos de entrada, ya que estos se crean dinámicamente. En su lugar, cada vez que se añade una instancia, genero el HTML desde el cóódigo asignando un "id" y un "name" secuencial del tipo "acto-xx", siendo "xx" un nro. secuencial. De la misma forma, al borrar el elemento, lo elimino del DOM (no lo oculto)

De esta forma, después puedo recopilar en una matriz todos los elementos del DOM que tiene ese "id" o ese "name".

Así, antes de proceder al envío de los datos al servidor, tengo las siguientes matrices de objetos:
datos orig-> (5) […]
- 0: Object { id: 59, even_titulo: "xxxxxxxxx", even_desc: "xxxxxxxxx.", … }
- 1: Object { id: 68, even_padre: 59, even_titulo: "xxxxx", … }
- 2: Object { id: 77, even_padre: 59, even_titulo: "xxxxx", … }
    3: Object { id: 82, even_padre: 59, even_titulo: "xxxxxxx", … }
    4: Object { id: 108, even_padre: 59, even_titulo: "xxxxxxx", … }

datos a grabar-> (6) […]​
    0: Object { id: 59, even_titulo: "xxxxxxxxx", even_desc: "xxxxxxxxx.", … }
    1: Object { id: 77, even_padre: 59, even_titulo: "xxxxx", … }
    2: Object { id: 82, even_padre: 59, even_titulo: "xxxxxxx", … }
    3: Object { id: 108, even_padre: 59, even_titulo: "xxxxxxx", … }
    4: Object { desc: "xxxxxxx", even_estado: 12, id: "nuevo-0", … }
    5: Object { desc: "xxxxx", even_estado: 12, id: "nuevo-2", … }

Se puede observar que se ha eliminado la instancia con id=68 y se han añadido las instancias con id="nuevo-0" y "nuevo-1"

a1-> 
Array(5) [ 59, 68, 77, 82, 108 ]
 a2-> 
Array(6) [ "nuevo-2", "nuevo-1", 59, 77, 82, 108 ]

ins-> 
Array [ "nuevo-2", "nuevo-1" ]
 upd-> 
Array(4) [ 59, 77, 82, 108 ]
 del-> 
Array [ 68 ]